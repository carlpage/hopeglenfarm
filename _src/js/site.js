// Executes the function when DOM will be loaded fully
console.log('in js');
$(document).ready(function () {
    // hover property will help us set the events for mouse enter and mouse leave
        $('.navigation li').hover(
            // When mouse enters the .navigation element
            function () {
                //Fade in the navigation submenu
                $('ul', this).fadeIn(); // fadeIn will show the sub cat menu
            },
            // When mouse leaves the .navigation element
            function () {
                //Fade out the navigation submenu
                $('ul', this).fadeOut(); // fadeOut will hide the sub cat menu
            }
        );


});

// $(document).ready(function() {
//     $('.navigation li a:not(:only-child)').click(function (e) {
//         $(this).siblings('.nav-dropdown').toggle();
        // Close one dropdown when selecting another
//         $('.nav-dropdown').not($(this).siblings()).hide();
//         e.stopPropagation();
//     });

//     $('html').click(function () {
//         $('.nav-dropdown').hide();
//     });
// });

$(document).ready(function () {
    // var isNavVisible = $(".navigation li:nth-child(n+3)").is(":visible");
    // var isNavHidden = $(".navigation li:nth-child(n+3)").is(":hidden");
    // var isIconsVisible = $(".icons").is(":visible");
    // var isIconsHidden = $(".icons").is(":hidden");

    $('.fa-bars').on('click', function () {
        $('.navigation li:nth-child(n+3)').toggle();
        $('.icons').toggle();
    });
});

$(document).ready(function () {
    (function () {
        var slider = '#cycloneslider-main-1';
        jQuery(slider + ' .cycloneslider-slides').cycle({
            fx: "fade",
            speed: 1000,
            timeout: 40000,
            pager: jQuery(slider + ' .cycloneslider-pager'),
            prev: jQuery(slider + ' .cycloneslider-prev'),
            next: jQuery(slider + ' .cycloneslider-next'),
            slideExpr: '.cycloneslider-slide',
            slideResize: false,
            pause: true
        });
    })();



});